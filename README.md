# CellTypeGraph Benchmark
![](resources/overview.png)

CellTypeGraph is a new graph benchmark for node classification.

## Benchmark Overview

### The dataset
The benchmark is distilled from of 84 *Arabidopsis* ovules segmentations, 
and the task is to classify each cell with its specific cell type.
We represent each specimen as a graph, where each cell is a node and any two adjacent cells are connected with an edge.
This python-package comes with a Pytorch DataLoader, and pre-computed node and edge features. But the latter can be 
fully customized and modified. The source data for CellTypeGraph Benchmark can be also manually download from
[zenodo.org](https://zenodo.org/record/6374104).


### Dependencies
- python >= 3.8
- tqdm
- h5py
- requests
- pyyaml
- numba
- pytorch
- torchmetrics
- pytorch-geometric

### Optional Dependencies (for running the examples):
- class_resolver

## Install CellTypeGraph Benchmark using conda
- for cuda 11.3
```
conda create -n ctg -c rusty1s -c pytorch -c conda-forge -c lcerrone ctg-benchmark cudatoolkit=11.3
```

## Simple training example
* A simple GCN training example can be found in [examples](examples/gcn_example.py).

